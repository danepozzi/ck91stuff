TotalMix {
	var <>netAddr;

	*new {
		^super.new.init()
	}

	init {
		netAddr = NetAddr.new("127.0.0.1", 7001);
	}

	mic {
		arg mic;
		netAddr.sendMsg("/1/busInput", 1.0);
		netAddr.sendMsg("/setBankStart", mic);
	}

	selectOut {
		netAddr.sendMsg("/1/busOutput", 1.0);
		netAddr.sendMsg("/setBankStart", 0.0);
	}

	gain {
		arg mic, start, end, dur=5, steps=120;
		var delta = (start-end).abs;
		var inc = delta/steps;

		if(end>start){
			fork{
				steps.do({ arg i;
					this.mic(mic);
					netAddr.sendMsg("/2/gain", start+(inc*i));
					(dur/steps).wait;
				})
			}
		}{
			fork{
				steps.do({ arg i;
					this.mic(mic);
					netAddr.sendMsg("/2/gain", start-(inc*i));
					(dur/steps).wait;
				})
			}
		}
	}

	gain1 {
		arg mic, gain;
		this.mic(mic);
		netAddr.sendMsg("/2/gain", gain);
	}

	phantom {
		arg mic;
		this.mic(mic);
		netAddr.sendMsg("/2/phantom", 1.0);
	}

	eqEnable {
		arg mic;
		this.mic(mic);
		netAddr.sendMsg("/2/eqEnable", 1.0);
	}

	eqGain {
		arg mic, gain= #[0.5,0.5,0.5];
		this.mic(mic);
		netAddr.sendMsg("/2/eqGain1", gain[0]);
		netAddr.sendMsg("/2/eqGain2", gain[1]);
		netAddr.sendMsg("/2/eqGain3", gain[2]);
	}

	volume {
		arg out, volume;
		this.selectOut;
		netAddr.sendMsg("/1/volume" ++ out.asInt, volume);
	}
}