ComplexMod {
	*ar {
		arg in1, in2, freq = 10000;
		var cos = [SinOsc.ar(freq,0.5*pi), SinOsc.ar(freq)];
		cos = [cos[0]*in1, cos[1]*in2];
		cos = [cos[0]-cos[1], cos[0]+cos[1]];
		^cos;
	}
}

ComplexModOld {
	*ar {
		arg in1, in2, freq = 10000;
		var sr = SampleRate.ir;
		var trig = BlitB3.ar(freq);
		var phasor = Phasor.ar(trig, freq/sr);
		var cos = [(phasor*2*pi).cos, ((phasor-0.25)*2*pi).cos];
		cos = [cos[0]*in1, cos[1]*in2];
		cos = [cos[0]-cos[1], cos[0]+cos[1]];
		^cos;
	}
}

HilbertShift {
	*ar {
		arg in, freq = 15000;
		var cos = [SinOsc.ar(freq,0.5*pi), SinOsc.ar(freq)];
		var hb = Hilbert.ar(in);
		var sig = hb*cos;
		sig = [sig[0]-sig[1], sig[0]+sig[1]];
		^sig;
	}
}

HilbertShiftOld {
	*ar {
		arg in, freq = 15000;
		var sr = SampleRate.ir;
		var trig = BlitB3.ar(freq);
		var phasor = Phasor.ar(trig, freq/sr);
		var cos = [(phasor*2*pi).cos, ((phasor-0.25)*2*pi).cos];
		var hb = Hilbert.ar(in);
		var sig = hb*cos;
		sig = [sig[0]-sig[1], sig[0]+sig[1]];
		^sig;
	}
}

HilbertShiftLeft {
	*ar {
		arg in, freq = 15000;
		var cos = [SinOsc.ar(freq,0.5*pi), SinOsc.ar(freq)];
		var hb = Hilbert.ar(in);
		var hb2 = Hilbert.ar(hb);
		var sig = hb*cos;
		sig = sig[0]-sig[1];
		^sig;
	}
}

HilbertShiftLeftOld {
	*ar {
		arg in, freq = 15000;
		var sr = SampleRate.ir;
		var trig = BlitB3.ar(freq);
		var phasor = Phasor.ar(trig, freq/sr);
		var cos = [(phasor*2*pi).cos, ((phasor-0.25)*2*pi).cos];
		var hb = Hilbert.ar(in);
		var sig = hb*cos;
		sig = sig[0]-sig[1];
		^sig;
	}
}

//{Phasor.ar(Impulse.ar(15000),15000/44100)}.plot //nope
//{Phasor.ar(BlitB3.ar(15000),15000/44100)}.plot //phasor in pure data
//{Phasor.ar(0, 15000, 0, 2048)/2048}.plot