GainControlLag : UGen {
	*ar {
		arg in=0.0, target=0.5, samples=4410, lag=0.2;
		^this.multiNew('audio', in, target, samples, lag*SampleRate.ir)
	}
}