DwSample : UGen {
	*ar {
		arg in=0.0, depth=2.0, mul=1.0, add=0.0;
		^this.multiNew('audio', in, depth).madd(mul, add)
	}
}