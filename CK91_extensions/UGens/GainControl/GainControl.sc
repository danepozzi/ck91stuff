GainControl : UGen {
	*ar {
		arg in=0.0, target=0.5, samples=4410;
		^this.multiNew('audio', in, target, samples)
	}
}