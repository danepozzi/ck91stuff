MeterView {

	classvar serverMeterViews, updateFreq = 10, dBLow = -80, meterWidth = 15, gapWidth = 4, <height = 230, w = 1164, h = 674;
	classvar serverCleanupFuncs;

	var <view;
	var inresp, outresp, synthFunc, responderFunc, server, numIns, numOuts, firstInBus, firstOutBus=0, inmeters, outmeters, startResponderFunc;

	*new { |aserver, numIns, numOuts, firstInBus, firstOutBus, meterWidth, gapWidth, height, w, h|
		^super.new.init(aserver, numIns, numOuts, firstInBus, firstOutBus, meterWidth, gapWidth, height, w, h)
	}

	*getWidth { arg numIns, numOuts, server;
		^3+((numIns + numOuts) * (meterWidth + gapWidth))
	}

	init {
		arg aserver, anumIns, anumOuts, afirstInBus, afirstOutBus, ameterWidth, agapWidth, aheight, aw, ah;
		var innerView, viewWidth, levelIndic, palette;

		server = aserver;

		firstInBus= afirstInBus;
		firstOutBus= afirstOutBus;

		numIns = anumIns ?? { server.options.numInputBusChannels };
		numOuts = anumOuts ?? { server.options.numOutputBusChannels };

		meterWidth = ameterWidth ?? {19};
		gapWidth = agapWidth ?? {1};
		height = aheight ?? {100};
		w = aw;
		h = ah;

		viewWidth= this.class.getWidth(anumIns, anumOuts);

		view = Window("", Rect(w, h, viewWidth, height) )
		.front.background_(Color.black.alpha = 0.15)
		.alwaysOnTop_(true);
		view.onClose_( { this.stop });

		inmeters= Array.newClear(numIns);
		outmeters= Array.newClear(numOuts);

		numIns.do({
			arg i;
			inmeters[i]= levelIndic = LevelIndicator( view,
				Rect((meterWidth+gapWidth)*i,
					0, meterWidth, height) ).warning_(0.9).critical_(1.0)
			.drawsPeak_(true)
			.numTicks_(0)
			.numMajorTicks_(0)
			.background_(Color.black.alpha = 0.15);

			StaticText(view, Rect((meterWidth+gapWidth)*i, height-15, meterWidth, 15))
			.stringColor_(Color.grey.alpha_(0.85))
			.font_(Font.sansSerif(9).boldVariant)
			.string_((i+firstInBus).asString);
		});

		numOuts.do({
			arg i;
			outmeters[i]= levelIndic = LevelIndicator(view,
				Rect(5+((meterWidth+gapWidth)*numIns)+((meterWidth+gapWidth)*i),
					0, meterWidth, height) ).warning_(0.9).critical_(1.0)
			.drawsPeak_(true)
			.numTicks_(0)
			.numMajorTicks_(0)
			.background_(Color.black.alpha = 0.15);

			StaticText(view, Rect(6+((meterWidth+gapWidth)*numIns)+((meterWidth+gapWidth)*i), height-15, meterWidth, 15))
			.font_(Font.sansSerif(9).boldVariant)
			.stringColor_(Color.grey.alpha_(0.85))
			.string_((i+firstOutBus).asString);
		});

		this.setSynthFunc(inmeters, outmeters);
		startResponderFunc = {this.startResponders};
		this.start;
	}

	setSynthFunc {
		var numRMSSamps, numRMSSampsRecip;

		synthFunc = {
			//responders and synths are started only once per server
			var numIns = server.options.numInputBusChannels;
			var numOuts = server.options.numOutputBusChannels;
			numRMSSamps = server.sampleRate / updateFreq;
			numRMSSampsRecip = 1 / numRMSSamps;

			server.bind( {
				var insynth, outsynth;
				if(numIns > 0, {
					insynth = SynthDef(server.name ++ "InputLevels", {
						var in = In.ar(NumOutputBuses.ir, numIns);
						SendPeakRMS.kr(in, updateFreq, 3, "/" ++ server.name ++ "InLevels")
					}).play(RootNode(server), nil, \addToHead);
				});
				if(numOuts > 0, {
					outsynth = SynthDef(server.name ++ "OutputLevels", {
						var in = In.ar(firstOutBus, numOuts);
						SendPeakRMS.kr(in, updateFreq, 3, "/" ++ server.name ++ "OutLevels")
					}).play(RootNode(server), nil, \addToTail);
				});

				if (serverCleanupFuncs.isNil) {
					serverCleanupFuncs = IdentityDictionary.new;
				};
				serverCleanupFuncs.put(server, {
					insynth.free;
					outsynth.free;
					ServerTree.remove(synthFunc, server);
				});
			});
		};
	}

	startResponders {
		var numRMSSamps, numRMSSampsRecip;

		//responders and synths are started only once per server
		numRMSSamps = server.sampleRate / updateFreq;
		numRMSSampsRecip = 1 / numRMSSamps;
		if(numIns > 0) {
			inresp = OSCFunc( {|msg|
				{
					try {
						var channelCount = min(msg.size - 3 / 2, numIns);

						channelCount.do {|channel|
							var baseIndex = 3 + (2*channel);
							var peakLevel = msg.at(baseIndex);
							var rmsValue = msg.at(baseIndex + 1);
							var meter = inmeters.at(channel);
							if (meter.notNil) {
								if (meter.isClosed.not) {
									meter.peakLevel = peakLevel.ampdb.linlin(dBLow, 0, 0, 1, \min);
									meter.value = rmsValue.ampdb.linlin(dBLow, 0, 0, 1);
								}
							}
						}
					} { |error|
						if(error.isKindOf(PrimitiveFailedError).not) { error.throw }
					};
				}.defer;
			}, ("/" ++ server.name ++ "InLevels").asSymbol, server.addr).fix;
		};
		if(numOuts > 0) {
			outresp = OSCFunc( {|msg|
				{
					try {
						var channelCount = min(msg.size - 3 / 2, numOuts);

						channelCount.do {|channel|
							var baseIndex = 3 + (2*channel);
							var peakLevel = msg.at(baseIndex);
							var rmsValue = msg.at(baseIndex + 1);
							var meter = outmeters.at(channel);
							if (meter.notNil) {
								if (meter.isClosed.not) {
									meter.peakLevel = peakLevel.ampdb.linlin(dBLow, 0, 0, 1, \min);
									meter.value = rmsValue.ampdb.linlin(dBLow, 0, 0, 1);
								}
							}
						}
					} { |error|
						if(error.isKindOf(PrimitiveFailedError).not) { error.throw }
					};
				}.defer;
			}, ("/" ++ server.name ++ "OutLevels").asSymbol, server.addr).fix;
		};
	}

	start {
		if(serverMeterViews.isNil) {
			serverMeterViews = IdentityDictionary.new;
		};
		if(serverMeterViews[server].isNil) {
			serverMeterViews.put(server, List());
		};
		if(serverMeterViews[server].size == 0) {
			ServerTree.add(synthFunc, server);
			if(server.serverRunning, synthFunc); // otherwise starts when booted
		};
		serverMeterViews[server].add(this);
		if (server.serverRunning) {
			this.startResponders
		} {
			ServerBoot.add (startResponderFunc, server)
		}
	}

	stop {
		serverMeterViews[server].remove(this);
		if(serverMeterViews[server].size == 0 and: (serverCleanupFuncs.notNil)) {
			serverCleanupFuncs[server].value;
			serverCleanupFuncs.removeAt(server);
		};

		(numIns > 0).if( { inresp.free; });
		(numOuts > 0).if( { outresp.free; });

		ServerBoot.remove(startResponderFunc, server)
	}

	remove {
		view.remove
	}
}

Meter {

	var <window, <meterView;

	*new { |server, numIns, numOuts|

		var window, meterView;

		numIns = numIns ?? { server.options.numInputBusChannels };
		numOuts = numOuts ?? { server.options.numOutputBusChannels };

		window = Window.new(server.name ++ " levels (dBFS)",
			Rect(5, 305, ServerMeterView.getWidth(numIns, numOuts), ServerMeterView.height),
			false);

		meterView = ServerMeterView(server, window, 0@0, numIns, numOuts);
		meterView.view.keyDownAction_( { arg view, char, modifiers;
			if(modifiers & 16515072 == 0) {
				case
				{char === 27.asAscii } { window.close };
			};
		});

		window.front;

		^super.newCopyArgs(window, meterView)

	}
}
