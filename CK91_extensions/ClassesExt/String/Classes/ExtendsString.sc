+ String {

	findNdef {
		arg ndef;
		var code = this;
		var start = code.find(ndef);
		var pointer = start;
		var proto  = code.copyRange(pointer, pointer+200);
		var counter = 1;
		var open = 0;
		var closed = 0;
		var end;

		while ( {counter != 0}, {

			open = proto.find("(");
			closed = proto.find(")");

			if (open.notNil){
				if (open<closed){
					counter = counter + 1;
					pointer = pointer + open + 1;
					proto = code.copyRange(pointer, pointer+200);
				}{
					counter = counter - 1;
					pointer = pointer + closed + 1;
					proto = code.copyRange(pointer, pointer+200);
				}
			}{
				counter = counter - 1;
				pointer = pointer + closed + 1;
				proto = code.copyRange(pointer, pointer+200);
			}
		});

		^ proto = code.copyRange(start-6, pointer);
	}

	findArgs {
		arg ndef;
		var start = this.find(ndef)-6;
		var proto  = this.findNdef(ndef);
		var kw = proto.find("arg");
		var semicolon = proto.find(";");

		^ proto = this.copyRange(start+kw-1, start+semicolon);
	}

	countArgs {
		arg ndef;
		var proto  = this.findArgs(ndef);
		var commas = proto.findAll(",");

		^ commas.size + 1;
	}

	findArg {
		arg ndef, index;
		var proto  = this.findArgs(ndef).postln;
		var commas = proto.findAll(",");
		var start, end, pointer, length;
		if (index==0){
			start = 4;
			end = commas[0];
		};
		if (index > commas.size){
			"argument out of bound".postln;
		}{
			start = commas[index-1];
			end = commas[index];
			pointer = Document.current.string.find(ndef)+11;
			length = end-start;
		};

		^ Document.current.selectRange(start+pointer -1, length);
	}

	copyArg {
		arg ndef, index;
		var proto  = this.findArgs(ndef).postln;
		var commas = proto.findAll(",");
		var start = commas[index-1];
		var end = commas[index];
		var pointer = Document.current.string.find(ndef)+11;
		var length = end-start;
		var spaces, argName;

		proto = Document.current.string.copyRange(start+pointer -1, length);
		spaces = proto.findAll(" ");
		if (spaces.size>1) {
			argName = proto.copyRange(1, spaces[1])
		}{
			argName = proto.copyRange(1, proto.size -1)
		};
		^ argName;
	}

	replaceArg {
		arg ndef, index, newArg;
		var proto  = this.findArg(ndef,index);
		proto.selectedString_(newArg);
	}
}