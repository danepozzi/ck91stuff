//hadamard matrix (butterfly algorithm) for reverberation with 16 delay lines
//d.p_june2019

Hadamard16 {
	*initClass {
		StartUp.add{
			~hadamard = {
				arg array = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], iterations = 2;
				var size = array.size;
				array = iterations.collect({
					arg index;
					var first = (size/iterations*index);
					var second = (size/iterations*(index + (index.even.asInt-0.5*2)));
					var sign = (((index+1%2)-0.5)*2);
					(size/iterations).asInt.collect({
						arg i;
						array[first+i]+(sign*array[second+i])
					})
				});
				array.flatten
			}
		}
	}

	*ar {
		arg input, room = 1.5, decay = 0.7, wet = 1;
		var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]**room/1000;
		var local = LocalIn.ar(16);
		var delay = DelayC.ar(local+input, 2, lengths);
		var had = delay;
		had = had * (decay*0.25);
		4.do({
			arg idx;
			var iterations = 2.pow(idx+1);
			had = ~hadamard.value(had, iterations.asInt);
		});
		had;
		LocalOut.ar(LPF.ar(had, 2000));
		^delay.sum/16;
	}
}