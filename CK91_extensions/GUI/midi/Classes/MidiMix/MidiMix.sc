MidiMix {
	var knobsMapArray, knobsMap, cc_knob, cc_numBox, cc_button, <>cc_slider, posx=750, posy=320,
	<>knobs, <>knobsSum, <>eq, <>eqFaders, <>buttons, <>faders, <>buttonsOn, <>fadersFunc, <>knobsFunc, <>buttonsOff, gate=1;

	*new {
		arg px=750, py=320;
		^super.new.init(px,py)
	}

	init {
		arg px, py;
		var mouseDown= false, offset=0;
		var gui_objects_width= 40;
		var mm_port = MIDIIn.findPort("MIDI Mix", "MIDI Mix");
		var nano_port = MIDIIn.findPort("nanoPAD2", "PAD");
		var wind, krouter, mode;

		("knobs are exponential").postln;

		posx = px;
		posy = py;
		wind = Window.new("", Rect(posx, posy, gui_objects_width*9-18, 340)).front.alwaysOnTop_(true);
		wind.view.decorator=FlowLayout(wind.view.bounds);
		wind.view.decorator.gap=2@2;
		wind.background= Color.black.alpha = 0.25;

		knobs= Bus.control(numChannels:24);		//knobs
		knobsSum= Bus.control(numChannels:8);	//knobs
		eq= Bus.control(numChannels:24);			//eq
		eqFaders= Bus.control(numChannels:9);			//eq
		buttons= Array.newClear(16);		    		//buttons
		buttonsOn= Array.newClear(16);
		buttonsOff= Array.newClear(16);
		fadersFunc= Array.newClear(9);
		knobsFunc= Array.newClear(24);
		faders= Bus.control(numChannels:9);		//faders
		cc_knob= 24.collect;
		cc_numBox= 48.collect;
		cc_button= 16.collect;
		cc_slider= 9.collect;

		knobsMapArray= Array.newClear(48);
		this.knobsMapping;

		//krouter = KnobRouter(300, 200, 3, 8, px: posx+500, py: posy+240).survive;
		24.do({arg i; eq.setAt(i, 0.5)});
		9.do({arg i; eqFaders.setAt(i, -30)});

		//KNOBS
		3.do{
			arg kn;
			8.do{
				arg knob;
				var which= knob+(kn*8);
				cc_knob[which]= Knob(
					parent:wind,
					bounds:gui_objects_width @ 15)
				.action_({
					arg obj;
					if(gate!=0){
						//if((obj.value-knobs.subBus(which+offset).getSynchronous).abs<0.05){
						knobs.setAt(which+offset, knobsMap[which].map(obj.value).post);

						if(knobsFunc[which+offset].notNil){
							knobsFunc[which+offset].value(knobsMap[which].map(obj.value));
						};


						"   |   ".post;

						if(which>15){
							knobsSum.setAt(which+offset-16,
								(
									knobsMap[which].map(obj.value)+
									knobsMap[which-8].map(knobs.subBus(which-8).getSynchronous)+
									knobsMap[which-16].map(knobs.subBus(which-16).getSynchronous)
							).postln)
						}{if(which>7){
							knobsSum.setAt(which+offset-8,
								(
									knobsMap[which].map(obj.value)+
									knobsMap[which+8].map(knobs.subBus(which+8).getSynchronous)+
									knobsMap[which-8].map(knobs.subBus(which-8).getSynchronous)
							).postln)
						}{
							knobsSum.setAt(which+offset,
								(
									knobsMap[which].map(obj.value)+
									knobsMap[which+8].map(knobs.subBus(which+8).getSynchronous)+
									knobsMap[which+16].map(knobs.subBus(which+16).getSynchronous)
							).postln)
						}
						}
						/*}{
						("MAGNET: "++knobs.subBus(which+offset).getSynchronous).postln;
						}*/
					}{
						//if((obj.value-eq.subBus(which+offset).getSynchronous.linlin(-40,40,0,1)).abs<0.05){
						eq.setAt(which+offset, obj.value.linlin(0,1,-40,40).postln);
						/*}{
						("MAGNET: "++eq.subBus(which+offset).getSynchronous.linlin(0,1,-40,40)).postln;
						}*/
					}
				})
				.mouseDownAction_{mouseDown=true}
				.mouseUpAction_{mouseDown=false};
			};
			8.do{
				arg map;
				2.do{
					arg mapp;
					cc_numBox[mapp+(map*2)+(kn*16)]= NumberBox(
						parent: wind,
						bounds:gui_objects_width/2.1@ 11)
					.font_(Font(size:6))
					.background_(Color.white.alpha = 0.45)
					.action_({
						arg num;
						knobsMapArray[mapp+(map*2)+(kn*16)]= num.value;
						this.knobsMapping;
					})
				}
			}
		};

		//BUTTONS
		16.do{
			arg button;
			cc_button[button]= Button(
				parent:wind,
				bounds:gui_objects_width @ 12

			)
			.states_([
				[""],
				["", Color.gray, Color.gray]])
			.action_({
				arg obj;
				//knob.postln;
				obj.value.postln;
				buttons[button+offset]= obj.value;
				if(obj.value==1){
					if(buttonsOn[button+offset].notNil)
					{
						buttonsOn[button+offset].value
					}
				}{
					if(buttonsOff[button+offset].notNil)
					{
						buttonsOff[button+offset].value
					}
				}
			})
		};

		//FADERS
		8.do{
			arg slider;
			var sld;
			cc_slider[slider]= EZSlider(
				parent: wind,
				bounds: Rect(0, 0, gui_objects_width, 180),
				label: "    " ++ (slider),
				controlSpec: \db,
				action: {
					arg obj;
					if(gate!=0){
						faders.setAt(slider+offset, obj.value.postln);
					}{
						eqFaders.setAt(slider+offset, (obj.value.dbamp**2).ampdb.postln);//GUADAGNO
					};

					if(fadersFunc[slider+offset].notNil)
					{
						fadersFunc[slider+offset].value((obj.value.dbamp**2).ampdb);
					}
				},
				unitWidth:30,
				initVal:-3,
				numberWidth:60,
				layout:\vert
			)
			.setColors(Color.grey,Color.white)
			.font_(Font("Helvetica",9))
			.sliderView.mouseDownAction_{mouseDown=true}
			.mouseUpAction_{mouseDown=false};

			sld=slider*2;
		};

		1.do{
			arg slider;
			var sld;
			cc_slider[8]= EZSlider(
				parent: wind,
				bounds: Rect(0, 0, 240, 30),
				controlSpec: \db,
				action: {
					arg obj;
					if(gate!=0){
						faders.setAt(8, obj.value.postln);
					}{
						eqFaders.setAt(8, obj.value.postln);
					}
				},
				unitWidth:30,
				initVal:-3,
				numberWidth:60,
				//layout:\vert
			)
			.setColors(Color.grey,Color.white)
			.font_(Font("Helvetica",9))
			.sliderView.mouseDownAction_{mouseDown=true}
			.mouseUpAction_{mouseDown=false};
		};

		Button(wind, Rect(0, 0, 29, 30))
		.states_([["n_box", Color.white, Color.grey]])
		.font_(Font("Helvetica",7))
		.action_({
			knobsMapArray.postln;
		});

		Button(wind, Rect(0, 0, 29, 30))
		.states_([["dump", Color.white, Color.grey]])
		.font_(Font("Helvetica",7))
		.action_({
			this.dump;
		});

		mode= Button(wind, Rect(0, 0, 29, 30))
		.states_([["CC", Color.white, Color.grey],["EQ", Color.white, Color.red]])
		.font_(Font("Helvetica",7))
		.action_({
			if(gate!=0){
				this.setGate(0);
				defer{24.do({arg i; cc_knob[i].centered_(true).value=eq.subBus(i).getSynchronous.linlin(-40,40,0,1)})};
				defer{9.do({arg i; cc_slider[i].value=eqFaders.subBus(i).getSynchronous.dbamp.sqrt})};
			}{
				this.setGate(1);
				defer{24.do({arg i; cc_knob[i].centered_(false).value=knobs.subBus(i).getSynchronous})};
				defer{9.do({arg i; cc_slider[i].value=faders.subBus(i).getSynchronous.dbamp.sqrt})};
			}
		});

		if(MIDIIn.findPort("MIDI Mix", "MIDI Mix").notNil){
			var case=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
			var midiout=[1,4,7,10,13,16,19,22,3,6,9,12,15,18,21,24];
			var noteout=[2,5,8,11,14,17,20,23];
			var mmx_out = MIDIOut.newByName("MIDI Mix", "MIDI Mix");

			//hello
			Task({
				256.do({ arg i;
					mmx_out.noteOn(0, midiout[i%16], 127);
					mmx_out.noteOn(0, 25, 0);
					mmx_out.noteOn(0, 26, 0);
					(0.4/(i+1)).wait;
					mmx_out.noteOn(0, midiout[15-i%16], 127);
					(0.4/(i+1)).wait;
					mmx_out.noteOn(0, midiout[i%16], 0);
					(0.4/(i+1)).wait;
					mmx_out.noteOn(0, midiout[15-i%16], 0);
					mmx_out.noteOn(0, 25, 0);
					mmx_out.noteOn(0, 26, 127);
				});
			}).play;

			//KNOBS
			MIDIdef.cc(\mm_k, {arg val, num, chan;
				//if(gate!=0){defer{cc_knob[num-16].valueAction=(val/127)}}
				//{defer{krouter.knobs[((num-16)/8).asInt,(num-16)%8].valueAction=(val/127)}}
				defer{cc_knob[num-16].valueAction=(val/127)};
			}, (16..48), srcID: mm_port.uid).permanent_(true);

			//BUTTONS
			MIDIdef.noteOn(\mm_ba, {arg val, num, chan;
				if (case[num-65] == 0){
					defer{cc_button[num-65].valueAction=127};
					case[num-65] = 1;
					mmx_out.noteOn(0, midiout[num-65], 127)
				} {
					defer{cc_button[num-65].valueAction=0};
					case[num-65] = 0;
					mmx_out.noteOn(0, midiout[num-65], 0)
				}
			}, (65..80), srcID: mm_port.uid).permanent_(true);

			MIDIdef.noteOn (\bankleft, {
				if(gate!=0){
					\left.postln;
					mmx_out.noteOn(0, 25, 127);
					mmx_out.noteOn(0, 26, 0);
					defer{mode.valueAction=1};
				}
			}, 25, srcID: mm_port.uid).permanent_(true);

			MIDIdef.noteOn (\bankright, {
				if(gate==0){
					\right.postln;
					mmx_out.noteOn(0, 25, 0);
					mmx_out.noteOn(0, 26, 127);
					defer{mode.valueAction=0};
				}
			}, 26, srcID: mm_port.uid).permanent_(true);

			//FADERS
			MIDIdef.cc(\mm_s, {arg val, num, chan;
				defer{cc_slider[num-1].valueAction=((val+1)/127)}//sens
				//defer{cc_slider[num-1].valueAction=((val+0.00001)/127)}//sens
			}, (1..9), srcID: mm_port.uid).permanent_(true);

			MIDIdef.cc(\mm_bb, {arg val, num, chan;
				mmx_out.noteOn(0, noteout[num-65], val)
			}, (65..73), srcID: mm_port.uid).permanent_(true);
		};
	}

	numBoxUpdate {
		cc_numBox.size.do({arg i; if(knobsMapArray[i].notNil){cc_numBox[i].value = knobsMapArray[i]}});
	}

	knobsMapping {
		arg ... knobs;
		knobsMap = 24.collect({arg i; ControlSpec(knobsMapArray[2*i],knobsMapArray[2*i+1],\exponential, 0.0001)});
		knobsMap.postln;
	}

	singleKnobMap {
		arg which, linear = true, lo, hi;
		if(linear){knobsMap[which] = ControlSpec(lo,hi,\lin)}{knobsMap[which] = ControlSpec(lo,hi,\exponential)}
	}

	setKnobs {
		arg ... knobs;
		knobs.size.do({ arg i;
			knobsMapArray[i]= knobs[i];
		});
		this.knobsMapping;
		this.numBoxUpdate;
	}

	load {
		arg ... values;

		var knobs = values.copyRange(0,23);
		var buttons = values.copyRange(24,39);
		var sliders = values.copyRange(40,47);

		cc_button.do({arg item; item.valueAction = 0});

		knobs.do({ arg item, i;
			defer{cc_knob[i].valueAction = item};
		});
		buttons.do({ arg item, i;
			if(item!=0){defer{cc_button[i].valueAction = item}};
		});
		sliders.do({ arg item, i;
			defer{cc_slider[i].valueAction = item};
		});
	}

	dump {
		cc_knob.do({ arg item, i;
			item.value.trunc(0.01).post;
			",".post;
		}); "".postln;
		cc_button.do({ arg item, i;
			item.value.trunc(0.01).post;
			",".post;
		}); "".postln;
		cc_slider.do({ arg item, i;
			item.value.trunc(0.01).post;
			",".post;
		}); "".postln;
	}

	setGate {
		arg newGate;
		gate = newGate;
	}
}