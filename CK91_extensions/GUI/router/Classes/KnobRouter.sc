KnobRouter {
	var firstBusSendNum=100, firstBusReturnNum=0, numInputs=2, numOutputs=2, wh=0, com=true, posx=1500, posy=400,
	<>knobs, oldKnobValue, oldKnobRescaled, threadBeats, oscReceiver, <>router, <>threshold, <>gain, cmdP, <>buf, ringArray, ring, <>knobFunc;

	*initClass {
		StartUp.add {
			this.initSynthDefs;
		}
	}

	*initSynthDefs {

		SynthDef(\routerK, {
			arg inputBus, physicalOutput, amp=0;
			var signal = In.ar(inputBus);
			//signal = DwSample.ar(signal, dw);
			Out.ar(physicalOutput, signal*amp);
		}).add;

		SynthDef(\read, {
			arg numOut, index;
			SendTrig.kr(Impulse.kr(20),index,InFeedback.ar(numOut));
		}).add;

		SynthDef(\ring, {
			arg inputBus, buffer;
			var signal = In.ar(inputBus);
			RecordBuf.ar(signal, buffer, loop: 1);
		}).add;
	}

	*new {
		arg firstBusSend=100, firstBusReturn=0, inputs=2, outputs=2, which=0, compander=true, px=1500, py=400;
		^super.new.init(firstBusSend,firstBusReturn,inputs,outputs,which,compander,px,py)
	}

	init {
		arg firstBusSend, firstBusReturn, inputs, outputs, which, compander, px, py;
		var sends = List.new;
		var e = topEnvironment;
		var w;

		knobFunc={};
		com = compander;
		firstBusSendNum = firstBusSend;
		firstBusReturnNum = firstBusReturn;
		numInputs = inputs;
		numOutputs = outputs;
		posx = px;
		posy = py;
		wh= which; //router index if having multiple routers operating on the same channel matrix
		w = Window.new("",Rect(posx, posy, 20*numOutputs+30+20+20+20, 20*numInputs+20)).front.alwaysOnTop_(true)
		.background_(Color.black.alpha = 0.15);
		knobs = Array2D.new(numInputs,numOutputs);
		oldKnobValue = Array2D.new(numInputs,numOutputs);
		oldKnobRescaled = Array2D.new(numInputs,numOutputs);
		threadBeats = Array2D.new(numInputs,numOutputs);
		router = Group.new(addAction:\addToTail);

		cmdP = {
			router = Group.new(addAction:\addToTail);
			defer{this.scan};
			defer{router.set(\threshold, (threshold.value*127+1/127).squared.squared.ampdb)};
			defer{router.set(\gain, ((gain.value*2-1)*20))};
			"survive".postln;
		};

		numInputs.do({
			arg index;
			var bus = firstBusSendNum+index;
			var numBox = NumberBox(w, Rect(0+10, index*20+10, 30, 20))
			.background_(Color.white.alpha = 0.25)
			.normalColor_(Color.white.alpha = 0.75)
			.value_(firstBusSendNum+index);

			numOutputs.do({
				arg i;
				oldKnobValue[index,i] = 0;
				oldKnobRescaled[index,i] = 0;
				threadBeats[index,i] = 0;
				knobs[index,i] = Knob(w, Rect(20*i+30+10, index*20+10, 20, 20))
				.font_(Font(size:9))
				.action_({
					arg knob;

					if (oldKnobValue[index,i]==0){
						threadBeats[index,i] = thisThread.beats;
						if(knob.value != 0){
							e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol.postln] =
							Synth(\routerK,
								[\inputBus, bus, \physicalOutput, i+firstBusReturnNum, \threshold, -30], router, \addToTail);
						}{}
					}{
						if (knob.value != 0){
							var delta = ((thisThread.beats-threadBeats[index,i])/
								(oldKnobValue[index,i]-knob.value).abs).clip(1,200).postln;
							var deltaSign = (knob.value-oldKnobValue[index,i]).sign;
							if (delta < 5){
								e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol].set(\amp, (knob.value).postln);
								oldKnobRescaled[index,i] = knob.value;
							}{
								e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol].set
								(\amp, (oldKnobRescaled[index,i]+(knob.value/delta*deltaSign)).postln);
								oldKnobRescaled[index,i] = oldKnobRescaled[index,i]+(knob.value/delta*deltaSign);
							}
						}{
							e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol].free;
							oldKnobRescaled[index,i] = 0;
						}
					};

					knobFunc.value;
					oldKnobValue[index,i] = knob.value;
					threadBeats[index,i] = thisThread.beats;
					//sends.postln;
					//(\bus ++ bus).postln;
				});
			});
		});

		threshold = Slider.new(w, Rect(20*numOutputs+30+15, 10, 20, 20*numInputs-20))
		.value_(-12.dbamp.sqrt.sqrt)
		.action_({
			arg slider;
			router.set(\threshold, (slider.value*127+1/127).squared.squared.ampdb.postln);
		});

		gain = Slider.new(w, Rect(20*numOutputs+50+15, 10, 20, 20*numInputs-20))
		.value_(-12.dbamp.sqrt.sqrt)
		.action_({
			arg slider;
			router.set(\gain, ((slider.value*2-1)*50).postln);
		});

		Button.new(w, Rect(20*numOutputs+30+15, 20*numInputs-10, 20, 20))
		.states_([["Ø", Color.white.alpha = 0.65, Color.grey]])
		.action_({
			this.post;
		});

		Button.new(w, Rect(20*numOutputs+50+15, 20*numInputs-10, 20, 20))
		.states_([["on", Color.white.alpha = 0.65, Color.grey],["off", Color.white.alpha = 0.65, Color.grey]])
		.font_(Font(size:9))
		.action_({
			arg butt;
			butt= 1- butt.value;
			router.set(\limiter, butt);
			("Limiter: "++ butt).postln;
		});

		w.onClose = {this.clear;router.free;CmdPeriod.remove(CmdPeriod.objects[CmdPeriod.objects.size-1])};
		w.front;
	}

	loadStereo {
		knobs[0,0].valueAction = 1;
		knobs[1,1].valueAction = 1;
		this.reload;
	}

	load {
		arg ... rout;
		var rout2D = Array2D.fromArray(numInputs,numOutputs,rout);
		var load = rout2D;
		this.clear;
		numInputs.do({arg index;
			numOutputs.do({arg i;
				knobs[index,i].value = load[index, i];
			})
		});
		this.scan;
	}

	clear {
		numInputs.do({arg index; numOutputs.do({arg i; knobs[index,i].value = 0})});
		router.freeAll;
	}

	scan {
		//knobs.postln;
		numInputs.do({arg index;
			numOutputs.do({arg i;
				if (knobs[index,i].value == 1){
					knobs[index,i].valueAction = 1;
				}
			})
		});
		this.reload;
	}

	survive {
		CmdPeriod.add({SystemClock.sched(0.1, cmdP)})
	}

	reload {
		numInputs.do({arg index;
			numOutputs.do({arg i;
				knobs[index,i].value;
			});
		});
	}

	post {
		("router ["++numInputs++"X"++numOutputs++"]").postln;
		numInputs.do({arg index;
			numOutputs.do({arg i;
				knobs[index,i].value.post;
				",".post;
			});
			"".postln;
		});
	}

	read {
		numOutputs.do({arg i; Synth(\read, [\numOut, firstBusReturnNum+i, \index, i])});
		oscReceiver = OSCFunc({
			arg msg, time;
			if(msg[2]!=(numOutputs-1)){msg[3].trunc(0.001).post;"	".post}{msg[3].trunc(0.001).postln};
		},'/tr');
	}

	ring {
		arg numFrames=4096;
		buf = Array.fill(numOutputs, Buffer.alloc(Server.default, numFrames));
		ring = Group.new(addAction:\addToTail);
		ringArray = Array.fill(numOutputs, {
			arg i;
			Synth(\ring, [\inputBus, i+firstBusReturnNum, \buffer, buf[i]], ring, \addToTail);
		});
	}

	clearRing {
		buf.do({arg item; item.free});
		buf = nil;
		ring.freeAll;
		ring.free;
	}
}