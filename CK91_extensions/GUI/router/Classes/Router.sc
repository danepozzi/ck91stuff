Router {
	var <>firstBusSendNum=100, <>firstBusReturnNum=0, <>numInputs=2, <>numOutputs=2, wh=0, com=true, posx=1500, posy=400,
	<>buttons, oscReceiver, <>router, <>threshold, <>gain, <>relax, cmdP, <>buf, ringArray, ring, <>buttFunc, <>asArray;

	*initClass {
		StartUp.add {
			this.initSynthDefs;
		}
	}

	*initSynthDefs {

		/*SynthDef(\router_comp, {
			arg inputBus, physicalOutput, threshold= -30, gain=0, relax=0.1, amp=0;
			var signal = InFeedback.ar(inputBus);
			//signal = DwSample.ar(signal, dw);
			signal= Compander.ar(signal, signal, threshold.dbamp, 1, inf.reciprocal, relaxTime:relax)*gain.dbamp;
			Out.ar(physicalOutput, (signal*amp.dbamp*Line.kr(0,1,0.1)));
		}).add;*/

		SynthDef(\router_c, {
			arg inputBus, physicalOutput, threshold= -30, gain=0, relax=0.1, limiter=0, lag=0.1;
			var signal = InFeedback.ar(inputBus);
			signal= Compander.ar(signal, signal, threshold.dbamp, 1, inf.reciprocal, relaxTime:relax);
			Out.ar(physicalOutput, signal*gain.lag(lag).dbamp);
		}).add;

		SynthDef(\router, {
			arg inputBus, physicalOutput, amp=0, lag=0, gain=0;
			var signal = In.ar(inputBus);
			//signal = DwSample.ar(signal, dw);
			Out.ar(physicalOutput, signal*amp.lag(lag).dbamp*gain.lag(lag).dbamp);
		}).add;


		SynthDef(\read, {
			arg numOut, index;
			SendTrig.kr(Impulse.kr(20),index,InFeedback.ar(numOut));
		}).add;

		SynthDef(\ring, {
			arg inputBus, buffer;
			var signal = In.ar(inputBus);
			RecordBuf.ar(signal, buffer, loop: 1);
		}).add;
	}

	*new {
		arg firstBusSend=100, firstBusReturn=0, inputs=2, outputs=2, which=0, compander=true, px=1500, py=400;
		^super.new.init(firstBusSend,firstBusReturn,inputs,outputs,which,compander,px,py)
	}

	init {
		arg firstBusSend, firstBusReturn, inputs, outputs, which, compander, px, py;
		var sends = List.new;
		var e = topEnvironment;
		var w;

		buttFunc={};
		com = compander;
		firstBusSendNum = firstBusSend;
		firstBusReturnNum = firstBusReturn;
		numInputs = inputs;
		numOutputs = outputs;
		asArray=Array.fill(numInputs*numOutputs, {0});
		posx = px;
		posy = py;
		wh= which; //router index if having multiple routers operating on the same channel matrix
		if(com){
			w = Window.new("",Rect(posx, posy, 20*numOutputs+30+20+20+20+20, 20*numInputs+20)).front.alwaysOnTop_(true)
			.background_(Color.black.alpha = 0.15);
		}{
			w = Window.new("",Rect(posx, posy, 20*numOutputs+30+20, 20*numInputs+20)).front.alwaysOnTop_(true)
			.background_(Color.black.alpha = 0.15);
		};
		buttons = Array2D.new(numInputs,numOutputs);
		router = Group.new(addAction:\addToTail);

		cmdP = {
			router = Group.new(addAction:\addToTail);
			defer{this.scan};
			defer{router.set(\threshold, (threshold.value*127+1/127).squared.squared.ampdb)};
			defer{router.set(\gain, ((gain.value*2-1)*20))};
			if(com){defer{threshold.valueAction= 0.1}};
			"survive".postln;
		};

		numInputs.do({
			arg index;
			var bus = firstBusSendNum+index;
			var numBox = NumberBox(w, Rect(0+10, index*20+10, 30, 20))
			.background_(Color.white.alpha = 0.25)
			.normalColor_(Color.white.alpha = 0.75)
			.value_(firstBusSendNum+index);

			numOutputs.do({
				arg i;
				buttons[index,i] = Button(w, Rect(20*i+30+10, index*20+10, 20, 20))
				.states_([
					[i+firstBusReturnNum, Color.white.alpha = 0.65, Color.grey],
					[i+firstBusReturnNum, Color.grey.alpha = 0.65, Color.green],
				])
				.font_(Font(size:9))
				.action_({
					arg butt;

					if(com){
						if (butt.value==1) {
							e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol.postln] =
							Synth(\router_c,
								[\inputBus, bus, \physicalOutput, i+firstBusReturnNum, \threshold, -30], router, \addToTail);
							router.set(\relax, relax.value);/////mod
							router.set(\threshold, (threshold.value*127+1/127).squared.squared.ampdb);//////mod
							router.set(\gain, (gain.value*2-1)*30);/////mod
						} {
							e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol].free
						};
					}{
						if (butt.value==1) {
							e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol.postln] =
							Synth(\router,
								[\inputBus, bus, \physicalOutput, i+firstBusReturnNum, \threshold, -30], router, \addToTail);
							asArray[bus-firstBusSendNum*numOutputs+i] = 1;
						} {
							e[("rout" ++ bus ++ "ch" ++ i ++ "w" ++ wh).asSymbol].free;
							asArray[bus-firstBusSendNum*numOutputs+i] = 1;
						};
					};

					buttFunc.value;
				});
			});
		});

		if(com){
			relax = Slider.new(w, Rect(20*numOutputs+30+15, 10, 20, 20*numInputs))
			.value_(0.1)
			.action_({
				arg slider;
				router.set(\relax, slider.value.postln);
			});

			threshold = Slider.new(w, Rect(20*numOutputs+50+15, 10, 20, 20*numInputs))
			.value_(-12.dbamp.sqrt.sqrt)
			.action_({
				arg slider;
				router.set(\threshold, (slider.value*127+1/127).squared.squared.ampdb.postln);
			});

			defer{threshold.valueAction= 0.1};

			gain = Slider.new(w, Rect(20*numOutputs+70+15, 10, 20, 20*numInputs))
			.value_(-12.dbamp.sqrt.sqrt)
			.action_({
				arg slider;
				router.set(\gain, ((slider.value*2-1)*240).postln);
			});

			Button.new(w, Rect(10, 10, 30, 20))
			.states_([["pst", Color.white.alpha = 0.65, Color.grey]])
			.action_({
				this.post;
			});
		}{
			Button.new(w, Rect(10, 10, 30, 20))
			.states_([["pst", Color.white.alpha = 0.65, Color.grey]])
			.action_({
				this.post;
			});
		};

		w.onClose = {this.clear;router.free;CmdPeriod.remove(CmdPeriod.objects[CmdPeriod.objects.size-1])};
		w.front;
	}

	loadStereo {
		buttons[0,0].valueAction = 1;
		buttons[1,1].valueAction = 1;
		this.reload;
	}

	load {
		arg ... rout;
		var rout2D = Array2D.fromArray(numInputs,numOutputs,rout);
		var load = rout2D;
		this.clear;
		numInputs.do({arg index;
			numOutputs.do({arg i;
				buttons[index,i].value = load[index, i];
			})
		});
		this.scan;
	}

	//interpreter crashes for arrays > 255 elements
	bug {
		arg ... bug;
		bug.postln;
	}

	clear {
		numInputs.do({arg index; numOutputs.do({arg i; buttons[index,i].value = 0})});
		router.freeAll;
	}

	scan {
		//buttons.postln;
		numInputs.do({arg index;
			numOutputs.do({arg i;
				if (buttons[index,i].value == 1){
					buttons[index,i].valueAction = 1;
				}
			})
		});
		this.reload;
	}

	survive {
		CmdPeriod.add({SystemClock.sched(0.1, cmdP)})
	}

	reload {
		numInputs.do({arg index;
			numOutputs.do({arg i;
				buttons[index,i].value;
			});
		});
	}

	post {
		("router ["++numInputs++"X"++numOutputs++"]").postln;
		numInputs.do({arg index;
			numOutputs.do({arg i;
				buttons[index,i].value.post;
				",".post;
			});
			"".postln;
		});
	}

	read {
		numOutputs.do({arg i; Synth(\read, [\numOut, firstBusReturnNum+i, \index, i])});
		oscReceiver = OSCFunc({
			arg msg, time;
			if(msg[2]!=(numOutputs-1)){msg[3].trunc(0.001).post;"	".post}{msg[3].trunc(0.001).postln};
		},'/tr');
	}

	ring {
		arg numFrames=4096;
		buf = Array.fill(numOutputs, Buffer.alloc(Server.default, numFrames));
		ring = Group.new(addAction:\addToTail);
		ringArray = Array.fill(numOutputs, {
			arg i;
			Synth(\ring, [\inputBus, i+firstBusReturnNum, \buffer, buf[i]], ring, \addToTail);
		});
	}

	clearRing {
		buf.do({arg item; item.free});
		buf = nil;
		ring.freeAll;
		ring.free;
	}
}