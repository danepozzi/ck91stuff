PeakCalibration {
	var
	<>micPeak0, <>micPeak1, <>micTarget0, <>micTarget1, <>mic0status, <>mic1status, <>synth0, <>synth1;

	*initClass {
		StartUp.add {
			this.initSynthDefs;
		}
	}

	*initSynthDefs {
		SynthDef(\micPeak0, {
			arg gain = 0;
			var max = RunningMax.ar(ARMS.ar(SoundIn.ar(0)*gain.dbamp));
			var prev = Delay1.ar(max);
			SendReply.ar(max>prev, '/max0', max);
		}).add;

		SynthDef(\micPeak1, {
			arg gain = 0;
			var max = RunningMax.ar(ARMS.ar(SoundIn.ar(1)*gain.dbamp));
			var prev = Delay1.ar(max);
			SendReply.ar(max>prev, '/max1', max);
		}).add;
	}

	*new {
		arg server=Server.local, isActive=true,  px=0, py=0;
		^super.new.init(server,isActive,px,py)
	}

	init {
		arg server, isActive, px, py;
		var w, start, reset, osc0, osc1;

		w = Window.new(bounds:Rect(px,py,482,35)).front.alwaysOnTop_(true);
		StaticText(w, Rect(10, 0, 200, 20)).string_("mic:0").font_("Courier");
		StaticText(w, Rect(10, 15, 200, 20)).string_("mic:1").font_("Courier");
		mic0status = StaticText(w, Rect(250, 0, 200, 20)).string_("<").font = "Courier";
		mic1status = StaticText(w, Rect(250, 15, 200, 20)).string_("<").font = "Courier";
		micPeak0 = StaticText(w, Rect(80, 0, 200, 20)).string_("Waiting for peak").font = "Courier";
		micPeak1 = StaticText(w, Rect(80, 15, 200, 20)).string_("Waiting for peak").font = "Courier";
		micTarget0 = StaticText(w, Rect(270, 0, 200, 20)).string_("Waiting for target").font = "Courier";
		micTarget1 = StaticText(w, Rect(270, 15, 200, 20)).string_("Waiting for target").font = "Courier";
		start = Button(w, Rect(450, 0, 33, 17.5)).states_([["off", Color.grey, Color.white], ["on", Color.white, Color.grey]]);
		reset = Button(w, Rect(450, 17.5, 33, 17.5)).states_([["reset", Color.grey, Color.white]]);

		start.action = {
			if (start.value==1){
				osc0 = OSCFunc({ |msg| defer{micPeak0.string = msg[3].asString} }, '/max0').permanent_(true);
				osc1 = OSCFunc({ |msg| defer{micPeak1.string = msg[3].asString} }, '/max1').permanent_(true);
				synth0 = Synth(\micPeak0, target:server);
				synth1 = Synth(\micPeak1, target:server);
				CmdPeriod.add({
					SystemClock.sched(0.1, {
						synth0 = Synth(\micPeak0, target:server);
						synth1 = Synth(\micPeak1, target:server);
					}
				)});
			}{
				osc0.free;
				osc1.free;
				synth0.free;
				synth1.free;
				CmdPeriod.remove(CmdPeriod.objects[CmdPeriod.objects.size-1])
			}
		};

		reset.action = {
			synth0.free;
			synth1.free;
			synth0 = Synth(\micPeak0, target:server);
			synth1 = Synth(\micPeak1, target:server);
		};

		if(isActive){start.valueAction = 1};

		w.onClose = {this.clear;CmdPeriod.remove(CmdPeriod.objects[CmdPeriod.objects.size-1])};
		w.front;
	}

	getPeaks {
		arg mic0, mic1;
		\mic0_.post; micPeak0.string.postln;
		\mic01_.post; micPeak1.string.postln;
	}

	setTarget {
		arg mic0, mic1;
		micTarget0.string = mic0.asString;
		micTarget1.string = mic1.asString;
	}
}