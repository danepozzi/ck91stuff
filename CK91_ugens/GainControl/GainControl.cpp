#include <SC_PlugIn.h>
#include <math.h>

static InterfaceTable *ft;

struct GainControl : public Unit {
	int msamp, mcount;
	float msum,msum2, mtarget;
	//float mmeanmult;
	float* msquares;
};

void GainControl_next(GainControl *unit, int inNumSamples);
void GainControl_Ctor(GainControl* unit);
void GainControl_Dtor(GainControl* unit);

void GainControl_Ctor( GainControl* unit )
{
	SETCALC(GainControl_next);

	unit->mtarget=ZIN0(1);
	unit->msamp= (int) ZIN0(2);

	//unit->mmeanmult= 1.0f/(unit->msamp);
	unit->msum=0.0f;
	unit->msum2=0.0f;
	unit->mcount=0; //unit->msamp-1;

	unit->msquares= (float*)RTAlloc(unit->mWorld, unit->msamp * sizeof(float));
	//initialise to zeroes
	for(int i=0; i<unit->msamp; ++i)
	unit->msquares[i]=0.f;
	OUT0(0) = 0.f;

}

void GainControl_Dtor(GainControl *unit)
{
	RTFree(unit->mWorld, unit->msquares);
}

void GainControl_next( GainControl *unit, int inNumSamples )
{
	float *in = ZIN(0);
	float *out = ZOUT(0);

	int count= unit->mcount;
	float samp= (float) unit->msamp;

	float * data= unit->msquares;
	float sum= unit->msum;

	float sum2= unit->msum2;
	float target= unit->mtarget;

	int todo=0;
	int done=0;
	while(done<inNumSamples) {
		todo= sc_min(inNumSamples-done,samp-count);

		for(int j=0;j<todo;++j) {
			sum -=data[count];
			float next= ZXP(in);
			float rms;
			float newGain;
			next = pow(next,2);
			data[count]= next;
			sum += next;
			sum2 +=next;
			rms = sqrt(1/samp*sum);
			if(rms>target){newGain=target/rms;}else{newGain=1.0f;};
			ZXP(out) = newGain;
			++count;
		}

		done+=todo;

		if( count == samp ) {
			count = 0;
			sum = sum2;
			sum2 = 0;
		}

	}

	unit->mcount =count;
	unit->msum =  sum;
	unit->msum2 =  sum2;
}

PluginLoad(GainControl){
    ft = inTable;
		DefineDtorUnit(GainControl);
}
