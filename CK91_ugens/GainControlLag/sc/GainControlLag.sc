GainControl : UGen {
	*ar {
		arg in=0.0, samples=4410, target=0.5, lag=0.2;
		^this.multiNew('audio', in, samples, target, lag)
	}
}