#include <SC_PlugIn.h>
#include <math.h>

static InterfaceTable *ft;

struct GainControlLag : public Unit {
	int msamp,mcount,mlag;
	float msum,msum2,mtarget,moldgain;
	//float mmeanmult;
	float* msquares;
};

void GainControlLag_next(GainControlLag *unit, int inNumSamples);
void GainControlLag_Ctor(GainControlLag* unit);
void GainControlLag_Dtor(GainControlLag* unit);

void GainControlLag_Ctor( GainControlLag* unit )
{
	SETCALC(GainControlLag_next);

	unit->mtarget= ZIN0(1);
	unit->msamp= (int) ZIN0(2);
	unit->mlag= (int) ZIN0(3);

	//unit->mmeanmult= 1.0f/(unit->msamp);
	unit->msum=0.0f;
	unit->msum2=0.0f;
	unit->mcount=0; //unit->msamp-1;
	unit->moldgain=0.0f;

	unit->msquares= (float*)RTAlloc(unit->mWorld, unit->msamp * sizeof(float));
	//initialise to zeroes
	for(int i=0; i<unit->msamp; ++i)
	unit->msquares[i]=0.f;
	OUT0(0) = 0.f;

}

void GainControlLag_Dtor(GainControlLag *unit)
{
	RTFree(unit->mWorld, unit->msquares);
}

void GainControlLag_next( GainControlLag *unit, int inNumSamples )
{
	float *in = ZIN(0);
	float *out = ZOUT(0);

	int lag= unit->mlag;
	int count= unit->mcount;
	float samp= (float) unit->msamp;

	float * data= unit->msquares;
	float sum= unit->msum;
	float sum2= unit->msum2;
	float target= unit->mtarget;

	int todo=0;
	int done=0;
	while(done<inNumSamples) {
		todo= sc_min(inNumSamples-done,samp-count);

		for(int j=0;j<todo;++j) {
			sum -=data[count];
			float next= ZXP(in);
			float rms;
			float newGain;
			float oldGain = unit->moldgain;
			float interp;
			next = pow(next,2);
			data[count]= next;
			sum += next;
			sum2 +=next;
			rms = sqrt(1/samp*sum);
			if(rms>target){newGain=target/rms;}else{newGain=1.0f;};
			interp = oldGain-((oldGain-newGain)/lag);
			ZXP(out) = interp;
			unit->moldgain = interp;
			++count;
		}

		done+=todo;

		if( count == samp ) {
			count = 0;
			sum = sum2;
			sum2 = 0;
		}

	}

	unit->mcount =count;
	unit->msum =  sum;
	unit->msum2 =  sum2;
}

PluginLoad(GainControlLag){
    ft = inTable;
		DefineDtorUnit(GainControlLag);
}
