//crude downsampler
#include <SC_PlugIn.h>
#include <math.h>

static InterfaceTable *ft;

struct ARMS : public Unit {
	int msamp, mcount;
	float msum,msum2;
	//float mmeanmult;
	float* msquares;
};

void ARMS_next(ARMS *unit, int inNumSamples);
void ARMS_Ctor(ARMS* unit);
void ARMS_Dtor(ARMS* unit);

void ARMS_Ctor( ARMS* unit )
{
	SETCALC(ARMS_next);

	unit->msamp= (int) ZIN0(1);

	//unit->mmeanmult= 1.0f/(unit->msamp);
	unit->msum=0.0f;
	unit->msum2=0.0f;
	unit->mcount=0; //unit->msamp-1;

	unit->msquares= (float*)RTAlloc(unit->mWorld, unit->msamp * sizeof(float));
	//initialise to zeroes
	for(int i=0; i<unit->msamp; ++i)
	unit->msquares[i]=0.f;
	OUT0(0) = 0.f;

}

void ARMS_Dtor(ARMS *unit)
{
	RTFree(unit->mWorld, unit->msquares);
}

//ARMS is easy because convolution kernel can be updated just by deleting oldest sample and adding newest-
//half hanning window convolution etc requires updating values for all samples in memory on each iteration
void ARMS_next( ARMS *unit, int inNumSamples )
{
	float *in = ZIN(0);
	float *out = ZOUT(0);

	int count= unit->mcount;
	int samp= unit->msamp;

	float * data= unit->msquares;
	float sum= unit->msum;
	//avoids floating point error accumulation over time- thanks to Ross Bencina
	float sum2= unit->msum2;

	int todo=0;
	int done=0;
	while(done<inNumSamples) {
		todo= sc_min(inNumSamples-done,samp-count);

		for(int j=0;j<todo;++j) {
			sum -=data[count];
			float next= ZXP(pow(in,2));
			data[count]= next;
			sum += next;
			sum2 +=next;
			ZXP(out) = sqrt((1/samp)*sum);
			++count;
		}

		done+=todo;

		if( count == samp ) {
			count = 0;
			sum = sum2;
			sum2 = 0;
		}

	}

	unit->mcount =count;
	unit->msum =  sum;
	unit->msum2 =  sum2;
}

PluginLoad(ARMS){
    ft = inTable;
		DefineDtorUnit(ARMS);
}
