//linearly interpolated downsampler
#include <SC_PlugIn.h>
#include <math.h>

static InterfaceTable *ft;

struct DwSample1 : public Unit
{
	float m_depth;
	float m_mem;
	float m_old;
	float m_step;
	int 	m_counter;
};

void DwSample1_Ctor(DwSample1 *unit);
void DwSample1_next(DwSample1 *unit, int inNumSamples);

void DwSample1_Ctor(DwSample1* unit)
{
	SETCALC(DwSample1_next);

	unit->m_counter = 1;
	unit->m_mem = IN(0)[0];
	unit->m_old = IN(0)[0];
	unit->m_step = 0.0;

	DwSample1_next(unit, 1);
}

void DwSample1_next(DwSample1 *unit, int inNumSamples)
{
	float *in = IN(0);		//IN(0) returns a float pointer to input 0
	float *out = OUT(0);	//float pointer to ouput 0
	float depth = IN0(1);	//same as IN(1)[0]
	float sampled;
	float val;
	float old;
	float step;
	int counter;

	for (int i=0; i < inNumSamples; i++){
    val = in[i];								//ar input
		counter = unit->m_counter;	//counter
		old = unit->m_old;					//previous value
		step = unit->m_step;
		if (counter%(int)depth) {
			unit->m_counter++;
			out[i] = old + (step*counter);
		} else {
			unit->m_counter = 1;
			unit->m_old = unit->m_mem;
			unit->m_mem = val;
			unit->m_step = (val - unit->m_old) / depth;
			out[i] = old + (step*counter);
		}
  }
}

PluginLoad(DwSample1){
    ft = inTable;
    DefineSimpleUnit(DwSample1);
}
