# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Daniele/Desktop/DwSample/DwSample.cpp" "/Users/Daniele/Desktop/DwSample/CMakeFiles/DwSample.dir/DwSample.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/Daniele/Documents/supercollider-Version-3.9.2/include/plugin_interface"
  "/Users/Daniele/Documents/supercollider-Version-3.9.2/include/common"
  "/Users/Daniele/Documents/supercollider-Version-3.9.2/common"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
