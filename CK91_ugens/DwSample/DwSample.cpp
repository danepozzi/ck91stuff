//crude downsampler
#include <SC_PlugIn.h>
#include <math.h>

static InterfaceTable *ft;

struct DwSample : public Unit
{
	float m_depth;
	float m_mem;
	int 	m_counter;
};

void DwSample_Ctor(DwSample *unit);
void DwSample_next(DwSample *unit, int inNumSamples);

void DwSample_Ctor(DwSample* unit)
{
	SETCALC(DwSample_next);

	unit->m_counter = 1;
	unit->m_mem = 0.0;

	DwSample_next(unit, 1);
}

void DwSample_next(DwSample *unit, int inNumSamples)
{
	float *in = IN(0);		//IN(0) returns a float pointer to input 0
	float *out = OUT(0);	//float pointer to ouput 0
	float depth = IN0(1);	//same as IN(1)[0]
	float sampled;
	float val;
	int counter;

	for (int i=0; i < inNumSamples; i++){
    val = in[i];								//ar input
		counter = unit->m_counter;	//counter
		sampled = unit->m_mem;			//downsampled
		if (counter%(int)depth) {
			unit->m_counter++;
			out[i] = sampled;
		} else {
			unit->m_counter = 1;
			unit->m_mem = val;
			out[i] = val;
		}
  }
}

PluginLoad(DwSample){
    ft = inTable;
    DefineSimpleUnit(DwSample);
}
